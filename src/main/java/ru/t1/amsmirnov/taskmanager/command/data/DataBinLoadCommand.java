package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.Domain;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.system.DumpFileException;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public final class DataBinLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-bin";

    @NotNull
    public static final String DESCRIPTION = "Load data from binary dump file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA BINARY LOAD]");
        try {
            @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BIN);
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            setDomain(domain);
        } catch (final Exception exception) {
            throw new DumpFileException(FILE_BIN, exception);
        }
    }

}

package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.ICommandRepository;
import ru.t1.amsmirnov.taskmanager.command.AbstractCommand;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractCommand> mapByName = new TreeMap<>();


    @Override
    public void add(@NotNull final AbstractCommand command) {
        @Nullable final String name = command.getName();
        if (name != null && !name.isEmpty()) mapByName.put(name, command);
        @Nullable final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument, command);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArgument(@NotNull final String argument) {
        return mapByArgument.get(argument);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return mapByName.get(name);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

}
